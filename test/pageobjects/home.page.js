

const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
    /**
     * define selectors using getter methods
     */
    get btnMasuk() {
        return $('=Masuk');
    }

    get btnProfile() {
        return $('button.avatar');
    }

    loggedEmail(email_valid) {
        return $(`//div[contains(text(),'${email_valid}')]`);
    }

    async clickOnProfile() {
        this.btnProfile.click();
    }

    async clickOnMasuk() {
        this.btnMasuk.click();
    }
}

module.exports = new HomePage();
