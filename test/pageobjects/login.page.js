

const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputEmail() {
        return $(`//input[@data-testid='TextInputAlmatEmailLogin']`);
    }

    get inputKataSandi() {
        return $(`//input[@data-testid='TextInputPasswordLogin']`);
    }

    get btnMasuk() {
        return $(`//button[@data-testid='ButtonMasukLogin']`);
    }

    get linkKirimUlangVerifikasi() {
        return $(`//div[contains(text(),'Kirim ulang email verifikasi')]`);
    }

    get errorStateInvalidEmail() {
        return $(`//div[@data-testid='ErrorStateEmailYangKamuMasukanTidakSesuaiLogin']`);
    }

    get errorStateBlankEmail() {
        return $(`//div[@data-testid='ErrorStateWajibMengisiAlamatEmailLogin']`);
    }

    get errorStateBlankKatasandi() {
        return $(`//div[@data-testid='ErrorStateWajibMengisiKataSandiLogin']`);
    }

    get errorInvalidCredential() {
        return $(`//span[@data-testid='MessageBoxAlamatEmailAtauKataSandiTidakSesuaiLogin']`);
    }

    get errorUnverifiedCredential() {
        return $(`//span[@data-testid='MessageBoxAkunKamuBelumTerverifikasiLogin']`);
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login(email, kata_sandi) {
        await browser.execute(`arguments[0].click()`, await this.inputEmail);
        await this.inputEmail.addValue(email);
        await browser.execute(`arguments[0].click()`, await this.inputKataSandi);
        await this.inputKataSandi.addValue(kata_sandi);
        await browser.execute("arguments[0].click();", await this.btnMasuk);
    }

    async resendVerification() {
        await this.linkKirimUlangVerifikasi.waitForDisplayed();
        await browser.execute(`arguments[0].click()`, await this.linkKirimUlangVerifikasi);
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('masuk');
    }
}

module.exports = new LoginPage();
