const LoginPage = require('../pageobjects/login.page');
const HomePage = require('../pageobjects/home.page');
const Data = require('../data/dataTest.json')

describe('Login feature', () => {
    it('Should not login using unverified credential', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_unverified, Data.katasandi);

        await expect(LoginPage.errorUnverifiedCredential).toBeDisplayed();
    });

    it('Should direct to verification page when click Kirim ulang email verifikasi', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_unverified, Data.katasandi);

        await LoginPage.resendVerification();

        //expect to direct verification page
    });

    it('Should not login using invalid email', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_invalid, Data.katasandi);

        await expect(LoginPage.errorStateInvalidEmail).toBeDisplayed();
    });

    it('Should not login using blank email', async () => {
        await LoginPage.open();
        await LoginPage.login('', Data.katasandi);

        await expect(LoginPage.errorStateBlankEmail).toBeDisplayed();
    });

    it('Should not login using blank katasandi', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_valid, '');

        await expect(LoginPage.errorStateBlankKatasandi).toBeDisplayed();
    });

    it('Should not login using invalid credential', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_valid, Data.katasandi_invalid);

        await expect(LoginPage.errorInvalidCredential).toBeDisplayed();
    });

    it('Should login with valid credentials', async () => {
        await LoginPage.open();
        await LoginPage.login(Data.email_valid, Data.katasandi);
        await HomePage.clickOnProfile();

        await expect(HomePage.loggedEmail(Data.email_valid)).toBeDisplayed();
        await expect(HomePage.loggedEmail(Data.email_valid)).toHaveTextContaining(Data.email_valid);
    });
});


